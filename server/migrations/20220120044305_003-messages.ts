import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('messages',(table)=>{
        table.increments('id');
        table.integer('sender_user_id');
        table.foreign('sender_user_id').references('users.id');
        table.integer('received_user_id');
        table.foreign('received_user_id').references('users.id');
        table.timestamp('create_time');
        table.string('content');
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('messages')
}

