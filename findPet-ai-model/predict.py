import imp
import tensorflow as tf
import os
import numpy as np
from label import getDataLabel

IMAGE_HEIGHT = 224
IMAGE_WIDTH = 224

labels = getDataLabel()
model = tf.keras.models.load_model("efficientnetV1B0Jan-24-2022_15_00_29.h5")


def predict(image_path: str) -> float:

    image = tf.keras.preprocessing.image.load_img(
        image_path, color_mode="rgb", target_size=(224, 224)
    )

    input_arr = (tf.keras.preprocessing.image.img_to_array(image) / 127.5) - 1
    input_arr = np.array([input_arr])

    prediction = model.predict(input_arr)
    # print(prediction)

    result = []
    for idx, prob in enumerate(prediction[0]):
      confidence = float(prob * 100)
      if confidence < 10:
        continue
      result.append({
        'image_class': labels[idx],
        'confidence': confidence
      })

    return result

    maxIndex = np.argmax(prediction)
    # print(labels[maxIndex])

    score = {}

    score["image_class"] = labels[maxIndex]
    score["confidence"] = int(prediction[0][maxIndex] * 100)

    return score
