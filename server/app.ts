import express from 'express';
import { PetController } from './controller/petController';
import { UserController } from './controller/userController';
import { knex } from './db';
import { isLoggedIn } from './guards';
import { PetService } from './services/petServices';
import { UserService } from './services/userService';
import cors from 'cors';
import { ChatRoomService } from './services/chatroomService';
import { ChatroomController } from './controller/chatroomController';
import { upload } from './upload';
import { sessionMiddleware } from './session';

let app = express();
app.use(
  cors({
    origin: 'http://localhost:3000',
    credentials: true,
  }),
);
app.use(express.urlencoded());
app.use(express.json());

app.use(sessionMiddleware);
app.get('/session', (req: express.Request, res: express.Response) => {
  res.json(req.session);
});

let userService = new UserService(knex);
let petService = new PetService(knex);
let chatroomService = new ChatRoomService(knex);
app.use(new UserController(userService).route);
app.use(new PetController(petService).route);
app.use(new ChatroomController(upload, chatroomService).route);
// app.use('/apples',isLoggedIn,appleRoutes);

app.use('/', express.static('uploads'));
app.use(express.static('public'));
app.use('/uploads', express.static('uploads'));
app.use(isLoggedIn, express.static('frontend'));
// app.use(attachSession)

let PORT = 8080;
app.listen(PORT, () => {
  console.log(`Listening at http://localhost:${PORT}/`);
});
