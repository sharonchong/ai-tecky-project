import { Request, Response } from 'express';
import express from 'express';
import { upload } from '../upload';
import { PetService } from '../services/petServices';
import fetch from 'node-fetch';
import '../session';

export class PetController {
  route = express.Router();
  constructor(private petService: PetService) {
    this.route.post('/pet', upload.single('photo'), this.uploadPet);
    this.route.post('/aiPredictPet', upload.single('photo'), this.aiPredictPet);
    this.route.get('/breedList', this.getBreedList);
    this.route.get('/pet/status/lost', this.listLostPets);
    this.route.get('/pet/status/found', this.listFoundPets);
  }

  listLostPets = async (req: Request, res: Response) => {
    try {
      let list = await this.petService.listLostPets();
      res.json({ list });
    } catch (error) {
      res.status(500).json({ error: String(error) });
    }
  };

  listFoundPets = async (req: Request, res: Response) => {
    try {
      let list = await this.petService.listLostPets();
      res.json({ list });
    } catch (error) {
      res.status(500).json({ error: String(error) });
    }
  };

  aiPredictPet = async (req: Request, res: Response) => {
    try {
      if (!req.file?.filename) {
        res.status(400).json({ error: 'missing photo' });
        return;
      }

      const result = await fetch('http://localhost:8000/', {
        method: 'POST',
        headers: {
          'content-type': 'application/json',
        },
        body: JSON.stringify({
          image_path: req.file?.filename,
        }),
      });

      const score = await result.json();

      res.json(score);
    } catch (error) {
      res.status(500).json({ error: String(error) });
    }
  };

  getBreedList = async (req: Request, res: Response) => {
    try {
      const result = await fetch('http://localhost:8000/');
      const json = await result.json();
      res.json(json);
    } catch (error) {
      res.status(500).json({ error: String(error) });
    }
  };

  uploadPet = async (req: Request, res: Response) => {
    if (!req.session.user) {
      res.status(400).json({ error: 'Please login before upload pet' });
      return;
    }
    try {
      const user_id = +req.session.user!.user_id;
      let { location, gender, breed, lost_time, found_time, register_time } =
        req.body;
      let image = req.file?.filename;
      // let userId = +req.session.user!.user_id;
      // if(!userId){
      //   res.status(400).json({ error: "please login first" });
      //   return;
      // }
      if (!image) {
        res.status(400).json({ error: 'missing photo' });
        return;
      }
      if (!location) {
        res.status(400).json({ error: 'missing location' });
        return;
      }
      if (!gender) {
        res.status(400).json({ error: 'fill in gender' });
        return;
      }
      if (!breed) {
        res.status(400).json({ error: 'fill in breed' });
        return;
      }
      if (!lost_time && !found_time && !register_time) {
        res
          .status(400)
          .json({ error: 'fill in lost_time or found_time or register_time' });
        return;
      }

      await this.petService.insertPet({
        image,
        location,
        gender,
        breed,
        lost_time,
        found_time,
        register_time,
        user_id,
      });
      res.json({ ok: true });
    } catch (error) {
      res.status(500).json({ error: String(error) });
    }
  };
}
