// export default function SignupPage() {
// }

import { FormEvent, useState } from 'react';
import { API_ORIGIN, post } from '../api';
import { ErrorMessage } from '../components/ErrorMessage';
import InputGroup from '../components/InputGroup';
import { PageProps } from './PageProps';

export const SignupPage = (props: PageProps) => {
  const [error, setError] = useState('');

  async function submit(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();
    let form = e.currentTarget;
    let json = await post('/user/register', {
      user_email: form.user_email.value,
      password: form.password.value,
      phone_num: form.phone_num.value,
    });
    console.log(json);
    setError(json.error);
    if (json.ok) {
      props.setTab('profile');
      props.setHasLogin(true)
    }
  }
  return (
    <div>
      <h1>Signup Page</h1>
      <form className="p-3" onSubmit={submit}>
        <InputGroup label="Email">
          <input type="text" name="user_email" className="form-control" />
        </InputGroup>

        <InputGroup label="Password">
          <input type="password" name="password" className="form-control" />
        </InputGroup>

        <InputGroup label="Phone no">
          <input type="tel" name="phone_num" className="form-control" />
        </InputGroup>

        <ErrorMessage error={error} />

        <InputGroup>
          <input type="submit" className="form-control"></input>
        </InputGroup>
      </form>
    </div>
  );
};
