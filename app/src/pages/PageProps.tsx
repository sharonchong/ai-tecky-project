export type PageProps = {
  setTab: (tab: string) => void;
  setHasLogin: (value: boolean) => void;
  hasLogin: boolean | null;
};
