from urllib import request
import tensorflow as tf
from sanic import Sanic
from sanic.response import json
import numpy as np
import pathlib
import os
from keras import utils
from predict import predict
import label


model = tf.keras.models.load_model('efficientnetV1B0Jan-24-2022_15_00_29.h5')
app = Sanic("FindPet-Ai")
data_dir = "dataset"
data_dir = pathlib.Path(data_dir)
# class_names=list(data_dir.glob('*/'))
img_height = 224
img_width = 224

@app.get("/")
async def getLabels(request):
    return json(label.getDataLabel())

@app.post("/")
async def callModel(request):
    content = request.json
    filename = content["image_path"]
    image_path = os.path.join('../server/uploads/', filename)
    # print(image_path)
    # return

    # url = f"http://localhost:8080/{filename}"
    # url = `http://localhost:8080/${filename}`

    # image_path = tf.keras.utils.get_file(
    #     fname=os.path.basename(filename), origin=url)

    print(image_path)

    # img = tf.keras.utils.load_img(
    #     image_path, target_size=(img_height, img_width)
    # )
    # img_array = tf.keras.utils.img_to_array(img)
    # img_array = tf.expand_dims(img_array, 0) # Create a batch

    # predictions = model.predict(img_array)

    predictions = predict(image_path)
    print(predictions)

    return json(predictions)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)
