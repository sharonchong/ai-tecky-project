import { FormEvent, useState } from 'react';
import { API_ORIGIN, post } from '../api';
import { ErrorMessage } from '../components/ErrorMessage';
import InputGroup from '../components/InputGroup';
import { PageProps } from './PageProps';

export const Login= (props:PageProps)=> {
    const [error, setError] = useState('');
    async function submit(e: FormEvent<HTMLFormElement>) {
        e.preventDefault();
        let form = e.currentTarget;
        let json = await post('/user/login', {
          user_email: form.user_email.value,
          password: form.password.value,
        });
        console.log(json);
        setError(json.error);
        if (json.ok) {
          props.setTab('profile');
          props.setHasLogin(true)
        }

  }
  return (
    <div>
      <h1>Login Page</h1>
      <form className="p-3" onSubmit={submit}>
        <InputGroup label="Email">
          <input type="text" name="user_email" className="form-control" />
        </InputGroup>

        <InputGroup label="Password">
          <input type="password" name="password" className="form-control" />
        </InputGroup>

        
        <ErrorMessage error={error} />

        <InputGroup>
          <input type="submit" className="form-control"></input>
        </InputGroup>
      </form>
    </div>
  );
}