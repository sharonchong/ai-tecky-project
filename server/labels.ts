import fs from 'fs';

export let labels = fs.readFileSync('labels.txt').toString()
    .split('\n')
    .map(label => label.replace('/', ''))
    .filter(label => label.length > 0)

// console.log(labels.slice(-5));
