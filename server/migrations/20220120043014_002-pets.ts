import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('pets',(table)=>{
        table.increments('id');
        table.integer('user_id');
        table.foreign('user_id').references('users.id');
        table.string('image');
        table.string('location');
        table.point('gps');
        table.boolean('gender');
        table.string('breed');
        table.timestamp('adopt_time');
        table.timestamp('lost_time');
        table.timestamp('found_time')
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('pets')
}

