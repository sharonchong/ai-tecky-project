import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.table('messages',table => {
        table.text('content').alter()
        table.string('image')
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('image')
    

}

