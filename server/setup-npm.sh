#!/bin/bash
set -e
set -o pipefail
set -x

## git
echo node_modules >> .gitignore
echo package-lock.json >> .gitignore
echo pnpm-lock.yaml >> .gitignore
echo yarn.lock >> .gitignore

## npm package
npm init --yes

## typescript
npm i -D ts-node ts-node-dev typescript @types/node
## tsconfig
echo '{
  "compilerOptions": {
    "strict": true,
    "module": "commonjs",
    "target": "es5",
    "lib": ["es6", "dom"],
    "sourceMap": true,
    "allowJs": true,
    "jsx": "react",
    "esModuleInterop": true,
    "moduleResolution": "node",
    "noImplicitReturns": true,
    "noImplicitThis": true,
    "noImplicitAny": true,
    "strictNullChecks": true,
    "suppressImplicitAnyIndexErrors": true,
    "noUnusedLocals": true
  },
  "exclude": ["node_modules", "build", "scripts", "index.js"]
}
' > tsconfig.json

## jest
npm i -D jest @types/jest ts-jest
npx ts-jest config:init
npm set-script test "jest"
npm set-script test:watch "jest --watch"

## express
npm i express
npm i -D @types/express

## env
npm i dotenv
echo .env >> .gitignore
npm i -D gen-env

## knex
npm i knex pg
npm i -D @types/pg
npx knex init -x ts
