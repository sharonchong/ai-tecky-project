import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('users',(table)=>{
        table.dropColumn('pet_image')
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('users',(table)=>{
        table.string('pet_image').nullable();
    })
}

