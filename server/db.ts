import Knex from "knex";
import { env } from "./env";

let configs = require("./knexfile");

export let knex = Knex(configs[env.NODE_ENV]);
