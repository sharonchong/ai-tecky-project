import { Knex } from "Knex";

export class PetService {
  constructor(private knex: Knex) {}

  async listLostPets() {
    let rows = await this.knex
      .select("*")
      .from("pets")
      .whereNotNull("lost_time");
    return rows;
  }

  async insertPet(row: {
    image: string;
    location: string;
    gender: boolean;
    breed: string;
    found_time?: string;
    lost_time?: string;
    register_time?: string;
    user_id: number;
  }) {
    await this.knex.insert(row).into("pets");
  }

}
