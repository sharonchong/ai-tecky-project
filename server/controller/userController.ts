import { Request, Response } from 'express';
import express from 'express';
import { UserService } from '../services/userService';
import { checkPassword } from '../hash';
import '../session';

export class UserController {
  route = express.Router();

  constructor(private userService: UserService) {
    this.route.post('/user/register', this.register2);
    this.route.post('/user/login', this.login);
    this.route.get('/user/profile', this.getProfile);
  }
  // register = async (req: Request, res: Response) => {
  //   try {
  //     if (!req.body.user_email) {
  //       res.status(400).json({ error: "missing user_email" });
  //       return;
  //     }
  //     if (!req.body.password) {
  //       res.status(400).json({ error: "missing password" });
  //       return;
  //     }
  //     if (!req.body.phone_num) {
  //       res.status(400).json({ error: "missing phone_num" });
  //       return;
  //     }
  //     await this.userService.register(req.body);
  //     res.json({ ok: true });
  //   } catch (error) {
  //     res.status(500).json({ error: String(error) });
  //   }
  // };

  getProfile = async (req: Request, res: Response) => {
    res.json(req.session.user || {});
  };

  register2 = async (req: Request, res: Response) => {
    try {
      let fields = ['user_email', 'password', 'phone_num'];
      for (let field of fields) {
        if (!req.body[field]) {
          res.status(400).json({ error: 'missing ' + field });
          return;
        }
      }
      let result = await this.userService.register(req.body);
      if (result === 'email used') {
        res.status(400).json({ error: 'This email is already used' });
      } else {
        req.session['user'] = {
          user_id: result,
          user_email: req.body.email,
        };
        res.json({ ok: true });
      }
    } catch (error) {
      res.status(500).json({ error: String(error) });
    }
  };

  login = async (req: Request, res: Response) => {
    const { user_email, password } = req.body;

    if (!user_email) {
      res.status(400).json({ error: 'missing email' });
      return;
    }
    if (!password) {
      res.status(400).json({ error: 'missing password' });
      return;
    }
    let foundUser = await this.userService.login(user_email);
    console.log(foundUser, password);
    if (!foundUser) {
      res.status(400).json({
        error: 'user email not exist',
      });
      return;
    }
    let isPasswordValid = await checkPassword(
      password,
      foundUser.password_hash,
    );
    if (!isPasswordValid) {
      delete req.session['user'];
      res.status(400).json({
        error: 'password is invalid',
      });
      return;
    }
    let user = foundUser;
    delete user.password_hash;
    req.session['user'] = {
      user_id: user.id,
      user_email: user.user_email,
    };

    res.json({
      ok: true,
      message: 'login success',
      foundUser,
    });
  };
}
