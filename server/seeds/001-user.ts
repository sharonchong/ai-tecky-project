import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("users").del();

    // Inserts seed entries
    await knex("users").insert([
        {  user_email: 'user@gmail.com',
        password_hash:'user',
        phone_num: '98765432'
        },
        {  user_email: 'tester@gmail.com',
        password_hash:'tester',
        phone_num: '97654321'
        }

    ]);
};
