export let API_ORIGIN = 'http://localhost:8080';

export async function post(url: string, body?: any) {
  let res = await fetch(API_ORIGIN + url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
    credentials: 'include',
  });
  let json = await res.json();
  return json;
}

export async function postForm(url: string, body: FormData) {
  let res = await fetch(API_ORIGIN + url, {
    method: 'POST',
    body,
    credentials: 'include',
  });
  let json = await res.json();
  return json;
}

export async function get(url: string) {
  let res = await fetch(API_ORIGIN + url, {
    credentials: 'include',
  });
  let json = await res.json();
  return json;
}
