import express from 'express';
import { ChatRoomService } from '../services/chatroomService';
import '../session';
import { Request, Response } from 'express';
import { Multer } from 'multer';
// import { Server as SocketIO } from "socket.io";

export class ChatroomController {
  route = express.Router();
  constructor(
    // private guards:{
    //     isLoggedIn:express.RequestHandler
    // },
    private upload: Multer,
    private chatroomService: ChatRoomService,
  ) // private io: SocketIO
  {
    this.route.get('/chatroom/loadMessages/:id', this.loadMessages);
    this.route.post(
      '/chatroom/addMessages',
      this.upload.single('photo'),
      this.addMessages,
    );
  }
  // defineAPI<Route extends string>(
  //     method: 'get' | 'put' | 'post',
  //     route: Route,
  //     ...routeHandlers: express.RequestHandler[]
  //   ) {
  //     let originalFn = routeHandlers.pop()!
  //     let fn = async (req: Request, res: Response, next: NextFunction) => {
  //       try {
  //         await originalFn(req, res, next)
  //       } catch (error: any) {
  //         if (error instanceof HttpError) {
  //           res.status(error.status).json({ message: error.message })
  //         } else {
  //           res.status(500).json({ message: error.toString()})
  //         }
  //       }
  //     }
  //     this.router[method](route, ...routeHandlers, fn)
  //     return fn
  //   }
  loadMessages = async (req: express.Request, res: express.Response) => {
    // let messagesId:String = req.params.id;
    // let messageHistoryId = Number(messagesId)

    let senderUser_id = req.session.user!.user_id;
    let receiverUser_id = req.params.id;
    const messageHistory = await this.chatroomService.loadMessages(
      senderUser_id,
      receiverUser_id,
    );
    console.log('message', messageHistory);
    res.json(messageHistory);
  };
  addMessages = async (req: Request, res: Response) => {
    const { content, receiverUser_id } = req.body;
    const file = req.file;
    const senderUser_id = +req.session.user!.user_id;
    let fileName = file ? file.filename : '';
    if (!content) {
      res.status(400).json('Please enter your messages');
      return;
    }
    await this.chatroomService.addMessages(
      content,
      fileName,
      senderUser_id,
      receiverUser_id,
    );
    // this.io.to('user:' + receiverUser_id).emit('new-message', {
    //   content,
    //   image: fileName,
    // });
    res.json({ 'Messages:': ' Sent!' });
  };
  // updateMessages = async (req: Request, res: Response) => {
  //   let messagesId = req.params.id;
  //   let messagesContent = req.body.content;
  //   await this.chatroomService.updateMessages(messagesContent, messagesId);
  //   res.json({ messages: "sent" });
  // };
}
