import { Knex } from 'knex';
import { hashPassword } from '../hash';

export class UserService {
  constructor(private knex: Knex) {}
  async register(row: {
    user_email: string;
    password: string;
    phone_num: string;
  }) {
    let existing_row = await this.knex
      .select('id')
      .from('users')
      .where({ user_email: row.user_email })
      .first();
    if (existing_row) {
      return 'email used' as const;
    }
    let rows = await this.knex
      .insert({
        user_email: row.user_email,
        password_hash: await hashPassword(row.password),
        phone_num: row.phone_num,
      })
      .into('users')
      .returning('id');
    return rows[0] as number;
  }
  async login(user_email: string) {
    // let rows = await this.knex.raw(/* sql */ `
    // SELECT id FROM users
    // WHERE(user_email = ? AND password = ?) `,[user_email,password])
    // return rows[0]

    const rows = await this.knex
      .select('*')
      .from('users')
      .where('user_email', '=', user_email);
    return rows[0];
  }
}
