export default function InputGroup(props: { label?: string; children: any }) {
  return (
    <div className="input-group mb-3">
      {props.label ? (
        <div className="input-group-prepend">
          <span className="input-group-text" id="inputGroup-sizing-default">
            {props.label}
          </span>
        </div>
      ) : null}
      {props.children}
    </div>
  );
}
