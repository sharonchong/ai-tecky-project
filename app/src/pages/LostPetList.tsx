import { useEffect, useState } from 'react';
import { API_ORIGIN, get } from '../api';
import { ErrorMessage } from '../components/ErrorMessage';
import './LostPetList.css';
import { PageProps } from './PageProps';

let petSample = {
  id: 1,
  user_id: null,
  image: 'photo-1643276790508-3.png',
  location: 'TW',
  gender: false,
  breed: 'king dog',
  register_time: null,
  lost_time: '2021-12-25T06:47:00.000Z',
  found_time: null,
};
type Pet = typeof petSample;

export default function LostPetList(props: PageProps) {
  const [list, setList] = useState<Pet[]>([]);
  const [error, setError] = useState('');
  async function loadList() {
    // let res = await fetch(API_ORIGIN + '/pet/status/lost');
    // let json = await res.json();
    let json = await get('/pet/status/lost');
    setList(json.list || []);
    setError(json.error);
  }
  useEffect(() => {
    loadList();
  }, []);
  return (
    <div className="LostPetList">
      <h2>Lost Pet List</h2>
      <button
        className="btn btn-primary"
        onClick={() => props.setTab('submit lost pet')}
        disabled={props.hasLogin != true}
      >
        Report Lost Pet
      </button>
      <ErrorMessage error={error} />
      <p>count: {list.length}</p>
      {list.map((pet) => (
        <div key={pet.id}>
          <div>#{pet.id}</div>
          <div>breed: {pet.breed}</div>
          <img src={`${API_ORIGIN}/uploads/${pet.image}`}></img>
        </div>
      ))}
    </div>
  );
}
