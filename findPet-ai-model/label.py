# import keras_efficientnet_v2
import tensorflow as tf
# from tensorflow import keras
# import tensorflow_datasets as tfds
# from tensorflow.keras import datasets,layers,models
# import matplotlib.pyplot as plt
# import numpy as np
import os
# from datetime import datetime

def getDataLabel():
    data_dir = "./dataset"
    # return os.listdir(data_dir)
    # to be confirmed model's batch_size
    batch_size =119  
    imgSize = 224

    splitRate = 0.2

    train_ds = tf.keras.preprocessing.image_dataset_from_directory(
    data_dir, seed=123, subset="training", validation_split=splitRate,
    image_size=(imgSize, imgSize), batch_size=batch_size
    )

    class_names = train_ds.class_names

    # print(class_names)

    return class_names