import {Request,Response,NextFunction} from 'express';
import './session'

export function isLoggedIn(req:Request,res:Response,next:NextFunction){
    if(req.session?.['user']){
        next();
    }else{
        res.redirect('/login.html');
    }
}