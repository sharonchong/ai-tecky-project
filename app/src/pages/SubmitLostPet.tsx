import { FormEvent, useEffect, useState } from 'react';
import { API_ORIGIN, get, post, postForm } from '../api';
import InputGroup from '../components/InputGroup';
import { PageProps } from './PageProps';

export default function SubmitLostPet(props: PageProps) {
  const [error, setError] = useState('');
  const [breedList, setBreedList] = useState(['']);
  const [predictResult, setPredictResult] = useState<
    | {
        image_class: string;
        confidence: number;
      }[]
    | null
  >([]);
  // let breedList: string[] = [];
  // for (let i = 1; i <= 120; i++) {
  //   breedList.push('dog ' + i);
  // }
  useEffect(() => {
    get('/breedList').then((json) => setBreedList(json));
  }, []);
  async function submit(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();
    let formData = new FormData(e.currentTarget);
    let json = await postForm('/pet', formData);
    console.log(json);
    setError(json.error);
    if (json.ok) {
      props.setTab('lost pet list');
    }
  }
  async function predictBreed(input: HTMLInputElement) {
    let file = input.files?.[0];
    if (!file) {
      setPredictResult([]);
      return;
    }
    setPredictResult(null);
    let formData = new FormData();
    formData.set('photo', file);
    let json = await postForm('/aiPredictPet', formData);
    setPredictResult(json);
  }
  return (
    <div>
      <h2>Submit Lost Pet</h2>
      ...
      <form encType="multipart/form-data" onSubmit={submit}>
        <InputGroup label="Location">
          <input type="text" name="location" className="form-control" />
        </InputGroup>

        <InputGroup label="Photo">
          <input
            type="file"
            name="photo"
            className="form-control"
            onChange={(e) => predictBreed(e.currentTarget)}
          />
        </InputGroup>

        <div className="alert alert-info">
          {predictResult == null
            ? 'AI is determining breed ...'
            : predictResult.length == 0
            ? 'No breed is detected'
            : predictResult
                .sort((a, b) => b.confidence - a.confidence)
                .map((result) => (
                  <div key={result.image_class}>
                    <b>{result.image_class}</b> ({result.confidence.toFixed(1)}
                    %)
                  </div>
                ))}
        </div>

        <InputGroup label="Breed">
          <input
            type="text"
            name="breed"
            className="form-control"
            list="breed-list"
          />
          <datalist id="breed-list">
            {breedList.map((breed) => (
              <option key={breed}>{breed}</option>
            ))}
          </datalist>
        </InputGroup>

        <InputGroup label="Gender">
          <select name="gender" className="form-control">
            <option value="f">Female</option>
            <option value="m">Male</option>
          </select>
        </InputGroup>

        <InputGroup label="Lost Time">
          <input type="datetime" name="lost_time" className="form-control" />
        </InputGroup>

        <div className="alert alert-danger" hidden={!error}>
          {error}
        </div>

        <div className="input-group">
          <input type="submit" className="form-control"></input>
        </div>
      </form>
    </div>
  );
}
