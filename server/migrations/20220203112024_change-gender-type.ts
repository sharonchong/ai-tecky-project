import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.alterTable('pets', (table) => {
    table.string('gender', 1).alter();
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable('pets', (table) => {
    table.boolean('gender').alter();
  });
}
