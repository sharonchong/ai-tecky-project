import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('users',(table)=>{
        table.increments('id');
        table.string('user_email').notNullable().unique();
        table.string('password_hash').notNullable();
        table.string('pet_image').nullable();
        table.string('phone_num').notNullable().unique();
        table.timestamps(false,true)

    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('users')
}

