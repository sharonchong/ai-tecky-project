export const ErrorMessage = (props: { error: string }) => {
  return (
    <div className="alert alert-danger" hidden={!props.error}>
      {props.error}
    </div>
  );
};
