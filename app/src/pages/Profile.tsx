import { useEffect, useState } from 'react';
import { get } from '../api';
import { PageProps } from './PageProps';

export function Profile(props: PageProps) {
  const [profile, setProfile] = useState<{
    user_id: number;
    user_email: string;
  } | null>(null);
  useEffect(() => {
    get('/user/profile').then((json) => {
      if (json.user_id) {
        setProfile(json);
      } else {
        props.setTab('login');
      }
    });
  }, []);
  const logout = () => {
    props.setHasLogin(false);
    props.setTab('login');
  };
  return (
    <div>
      <h2>Profile</h2>
      {profile ? (
        <div>
          <div>id: {profile.user_id}</div>
          <div>email: {profile.user_email}</div>
        </div>
      ) : (
        <p>loading...</p>
      )}
      <button className="btn btn-dark" onClick={logout}>
        Logout
      </button>
    </div>
  );
}
