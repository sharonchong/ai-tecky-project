from urllib import request
import tensorflow as tf
from sanic import Sanic
from sanic.response import json
import numpy as np
import pathlib
import os
imgPath = 'your/image/location/apple.jpg'
model = tf.keras.models.load_model('my_model.h5')
label = ["your" , "label", "regarding" , "the" , "training" , "label", "priority"]

image = tf.keras.preprocessing.image.load_img(
    imgPath, color_mode="rgb", target_size=(224,224)
)

input_arr = ( tf.keras.preprocessing.image.img_to_array(image) / 127.5) - 1
input_arr = np.array([input_arr])

prediction = model.predict(input_arr)
print(prediction)

maxIndex = np.argmax(prediction)
print(label[maxIndex])