import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.alterTable("pets", (table) => {
    table.renameColumn("adopt_time", "register_time");
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable("pets", (table) => {
    table.renameColumn("register_time", "adopt_time");
  });
}
