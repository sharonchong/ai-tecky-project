import { Knex } from 'knex';
// import { Mess`ages } from '../model'
export class ChatRoomService {
  constructor(private knex: Knex) {}

  async loadMessages(sender_user_id: number, received_user_id: string) {
    // const ids = await this.knex.raw(
    //   `select id from users where user_email = '${receiverUser_email}'`,
    // );
    // console.log(ids.rows)

    let query = this.knex
      .select('*')
      .from('messages')
      .orWhere({ sender_user_id, received_user_id })
      .orWhere({
        sender_user_id: received_user_id,
        received_user_id: sender_user_id,
      });
    console.log(query.toSQL());
    let messageHistory = await query;
    console.log(messageHistory);
    return messageHistory;
    /*

      SELECT * FROM messages
      WHERE (senderUser_id = ? AND receiverUser_id = ?) OR 
      (senderUser_id = ? AND receiverUser_id = ?)

      [senderUser_id, receiverUser_id, receiverUser_id, senderUser_id]
      [sender_user_id,${senderUser_id} ,${senderUser_id} ,senderUser_id]
      */
    // console.log(messageHistory.rows);
    // return messageHistory.rows;
  }
  // async loadMessages(id:number):Promise<Messages[]>{
  //     let messages:Messages[]=await this.table()
  //     .select('*')
  //     .from('messages')
  //     .where('senderUser_id',id)
  //     .orWhere('receiveUser_id',id)
  //     .orderBy('id','asc')
  //     return messages

  async addMessages(
    content: string,
    fileName: string,
    senderUser_id: number,
    receiverUser_id: number,
  ) {
    await this.knex.from('messages').insert({
      content: content,
      image: fileName,
      sender_user_id: senderUser_id,
      received_user_id: receiverUser_id,
    });
  }

  // async updateMessages(messagesId:number,messagesContent:string){
  //   await this.knex("messages").update("content",messagesContent).where("id",messagesId)
  // }
}
