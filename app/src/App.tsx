import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { Profile } from './pages/Profile';
import LostPetList from './pages/LostPetList';
import SubmitLostPet from './pages/SubmitLostPet';
import { SignupPage } from './pages/SignupPage';
import { PageProps } from './pages/PageProps';
import { Login } from './pages/Login';
import { get } from './api';

function App() {
  // let count = 1
  const [count, setCount] = useState(1);
  const [currentTab, setTab] = useState('signup');
  const [hasLogin, setHasLogin] = useState<boolean | null>(null);
  let pageProps: PageProps = { setTab, setHasLogin, hasLogin };
  useEffect(() => {
    setTimeout(() => {
      get('/user/profile').then((json) => {
        if (json.user_id) {
          setHasLogin(true);
          setTab('profile');
        } else {
          setHasLogin(false);
          setTab('signup');
        }
      });
    }, 1000);
  }, []);
  function inc() {
    // count ++
    setCount(count + 1);
  }
  function renderContent() {
    if (hasLogin == null) {
      return <div>Checking login ....</div>;
    }
    switch (currentTab) {
      case 'signup':
        return <SignupPage {...pageProps} />;

      case 'login':
        return <Login {...pageProps} />;

      case 'counter':
        return (
          <header>
            <h2>Counter</h2>
            Count: {count}
            <button onClick={inc}>+1</button>
            Count: {count}
          </header>
        );
      case 'profile':
        return <Profile {...pageProps}></Profile>;
      case 'lost pet list':
        return <LostPetList {...pageProps}></LostPetList>;
      case 'submit lost pet':
        return <SubmitLostPet {...pageProps} />;
    }
  }
  let tabs = hasLogin
    ? ['counter', 'profile', 'lost pet list']
    : ['signup', 'login', 'counter', 'lost pet list'];

  return (
    <div className="App">
      <div className="btn-group">
        {tabs.map((tab) => (
          <button
            key={tab}
            className={
              'btn border ' + (tab == currentTab ? 'btn-primary' : 'btn-info')
            }
            onClick={() => setTab(tab)}
          >
            {tab}
          </button>
        ))}
      </div>
      <div>tab: {currentTab}</div>
      {renderContent()}
    </div>
  );
}

export default App;
