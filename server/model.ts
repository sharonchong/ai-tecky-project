export type Users={
    id:number;
    user_email:string;
    password_hash:string;
    phone_num:string;
}

export type Pets = {
    id:number;
    user_id:number;
    image:string|null;
    location:string|null;
    gender:boolean;
    breed:string;
    adopt_time?:string|null;
    lost_time?:string|null;
    found_time?:string|null;
}
export type Messages={
    id:number;
    senderUser_id:number;
    receiverUser_id:number;
    content:Text;
    image:string;

}