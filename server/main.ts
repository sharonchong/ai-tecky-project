import express from "express";
import { PetController } from "./controller/petController";
import { UserController } from "./controller/userController";
import { knex } from "./db";
import { isLoggedIn } from "./guards";
import { PetService } from "./services/petServices";
import { UserService } from "./services/userService";
import cors from 'cors'
import { ChatRoomService } from "./services/chatroomService";
import { ChatroomController } from "./controller/chatroomController";
import { Server as SocketIOServer } from 'socket.io'
import { Server as HttpServer } from 'http'
import { upload } from "./upload";
import { setSocketIO } from "./socketIO";
import { sessionMiddleware } from './session'

let app = express();

let httpServer = new HttpServer(app)
let io = new SocketIOServer(httpServer)
io.on('connection', socket => {
  console.log('socket connected:', socket.id)
  const req = socket.request as express.Request;
  socket.join('user:' + req.session.user!.user_id)
  socket.on('/chatroom', id => {
    console.log(id)
  })
})
setSocketIO(io)

app.use(cors())

app.use(sessionMiddleware);

io.use((socket, next) => {
  let req = socket.request as express.Request
  let res = req.res as express.Response
  sessionMiddleware(req, res, next as express.NextFunction)
})



let userService = new UserService(knex);
let petService = new PetService(knex)
let chatroomService = new ChatRoomService(knex)
app.use(new UserController(userService).route);
app.use(new PetController(petService).route);
app.use(new ChatroomController(upload, chatroomService, io).route);
// app.use('/apples',isLoggedIn,appleRoutes);


app.use('/', express.static('uploads'))
app.use(express.static("public"));
app.use('/uploads', express.static("uploads"));
app.use(isLoggedIn, express.static("frontend"));

let PORT = 8080;
// app.listen(PORT, () => {
//   console.log(`listen to ${PORT}`);
// });
httpServer.listen(PORT, () => {
  console.log(`listen to ${PORT}`);
});


